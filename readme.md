# Protectorate

Game and game engine.

## Building

To build Protectorate, you will need the following software tools:

  * A D programming language compiler.

  * The Dub package manager.

Provided that the above pre-requesites are met, building is as follows:

  * Download or clone the repository.

  * Navigate to the folder using a command line interface.

  * Execute `dub build` or `dub run` within the folder to build or run.

  * Documentation may be generated using `./tools/docgen.sh`.
