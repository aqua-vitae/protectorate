/**
 * Type construct utilities for more expressive and safer programming practices.
 */
module aqvi.types;

private import std.traits;

/**
 * Lightweight wrapper around `Type`, which provides semantics for checking the existence of a
 * value in a type safe manner.
 *
 * For reference types, such as classes and pointers, the struct will utilize `null` to express an
 * empty state, allowing it to avoid any additional data packing.
 */
public struct Optional(Type) {
	private Type value_;

	static if (isPointer!Type || is(Type == class)) {
		/**
		 * Assigns a value of `value`, with a `null` value leaving the contents empty.
		 */
		public this(inout (Type) value) inout {
			this.value_ = value;
		}

		/**
		 * Returns `true` if a value is contained, otherwise `false`.
		 */
		public bool hasValue() const {
			return (this.value_ !is null);
		}
	} else {
		private bool hasValue_ = false;

		/**
		 * Assigns a value of `value`.
		 */
		public this(inout (Type) value) inout {
			this.hasValue_ = true;
			this.value_ = value;
		}

		/**
		 * Returns `true` if a value is contained, otherwise `false`.
		 */
		public bool hasValue() const {
			return this.hasValue_;
		}
	}

	/**
	 * Attempts to grab the contained value, asserting if it does not exist.
	 *
	 * It is recommended that [Optional.hasValue] be called first if the presence of a value
	 * cannot be assumed.
	 */
	public ref inout (Type) value() inout {
		assert(this.hasValue(), "Attempt to access empty Optional.");

		return this.value_;
	}
}

/**
 * Returns an [Optional] of `Type` containing no value.
 */
public Optional!Type none(Type)() {
	return Optional!Type();
}
