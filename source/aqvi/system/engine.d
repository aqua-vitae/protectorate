module aqvi.system.engine;

private import
	aqvi.collections.slotmap,
	aqvi.collections.stack,
	aqvi.image,
	aqvi.lina,
	aqvi.memory,
	aqvi.types,
	aqvi.system.assets,
	rl = raylib,
	std.math,
	std.string;

public alias DragHandler = @safe @nogc void delegate(Vector2 relativeDrag);

public alias ZoomHandler = @safe @nogc void delegate(float relativeZoom);

private enum LightType {
	point,
	directional,
}

/**
 * Viewport projection information.
 */
public struct Camera {
	/**
	 * Initial position in 3D world space.
	 */
	Vector3 origin = Vector3.zero;

	/**
	 * Observed position in 3D world space.
	 */
	Vector3 target = Vector3.zero;
}

/**
 * Opaque handle to a light source.
 */
public struct LightKey {
	private LightType type;

	private SlotKey data;
}

/**
 * Server of GPU-bound assets and dispatcher of draw commands.
 */
public final class Engine {
	private struct AnimatedDraw {
		Vector3 position;

		AnimatedModel model;
	}

	private struct PointLight {
		Vector3 origin;

		Vector4 color;

		@trusted @nogc
		void refresh(Engine engine, in uint index) {
			rl.SetShaderValue(
				engine.standardShader,

				rl.GetShaderLocation(
					engine.standardShader,
					rl.TextFormat("pointLights[%d].origin", index)
				),

				this.origin.values().ptr,
				rl.ShaderUniformDataType.UNIFORM_VEC3
			);

			rl.SetShaderValue(
				engine.standardShader,

				rl.GetShaderLocation(
					engine.standardShader,
					rl.TextFormat("pointLights[%d].color", index)
				),

				this.color.values().ptr,
				rl.ShaderUniformDataType.UNIFORM_VEC4
			);
		}
	}

	private struct StaticDraw {
		StaticModel model;

		Matrix transform;
	}

	private enum pointLightMax = 4;

	package static rl.Shader standardShader;

	private auto pointLights = new PackedSlotMap!PointLight(Allocator.persistent);

	private auto animatedDraws = new PackedStack!AnimatedDraw(Allocator.persistent);

	private auto staticDraws = new PackedStack!StaticDraw(Allocator.persistent);

	private auto dragHandlers = new PackedStack!DragHandler(Allocator.persistent);

	private auto zoomHandlers = new PackedStack!ZoomHandler(Allocator.persistent);

	private Vector2 dragPosition = Vector2.zero;

	/**
	 * Attempts to create a point light positioned at `origin` with a light value of `color`.
	 *
	 * A [LightKey] is returned wrapped in an [Optional], with [none] indicating that there are no
	 * more lights available to be handed out.
	 */
	@trusted @nogc
	public Optional!LightKey createPointLight(in Vector3 origin, in Color color) {
		if (this.pointLights.count() < pointLightMax) {
			immutable (LightKey) key = {
				type: LightType.point,
				data: this.pointLights.insert(PointLight(origin, color.normalized())),
			};

			this.refreshPointLights();

			return Optional!LightKey(key);
		}

		return none!LightKey();
	}

	/**
	 * Clears all point lights.
	 */
	@trusted @nogc
	public void clearPointLights() {
		this.pointLights.clear();
		this.refreshPointLights();
	}

	/**
	 * Deletes the point light at `lightKey`.
	 *
	 * An assertion is raised if `lightKey` does not reference a point light.
	 */
	@trusted @nogc
	public void deletePointLight(in LightKey lightKey) {
		assert(
			lightKey.type == LightType.point,
			"Attempt to delete non-point light using GraphicsServer.deletePointLight"
		);

		this.pointLights.remove(lightKey.data);
		this.refreshPointLights();
	}

	/**
	 * Draws `model` at `position` in world space.
	 */
	public void drawModel(AnimatedModel model, in Vector3 position) {
		this.animatedDraws.push(AnimatedDraw(position, model));
	}

	/**
	 * Draws `model` at `position` in world space.
	 */
	public void drawModel(StaticModel model, in Matrix transform) {
		this.staticDraws.push(StaticDraw(model, transform));
	}

	/**
	 * Attempts to initialize and return a [GraphicsServer] instance with `width` by `height`
	 * dimensions and `title` as the display title, returning [none] if it failed to initialize or
	 * one already exists.
	 */
	@trusted
	public static Optional!Engine initialize(in int width, in int height, in string title) {
		if (!rl.IsWindowReady()) {
			enum defaultWidth = 1280;
			enum defaultHeight = 720;

			rl.SetConfigFlags(rl.ConfigFlag.FLAG_MSAA_4X_HINT);

			rl.InitWindow(
				(width == 0) ? defaultWidth : width,
				(height == 0) ? defaultHeight : height,
				toStringz(title)
			);

			if (rl.IsWindowReady()) {
				standardShader = rl.LoadShaderCode(
					import("aqvi/shaders/standard.vert"),
					import("aqvi/shaders/standard.frag")
				);

				standardShader.locs[rl.ShaderLocationIndex.LOC_MATRIX_MODEL] =
					rl.GetShaderLocation(standardShader, "matModel");

				standardShader.locs[rl.ShaderLocationIndex.LOC_VECTOR_VIEW] =
					rl.GetShaderLocation(standardShader, "viewPos");

				return Optional!Engine(new Engine());
			}
		}

		return none!Engine();
	}

	/**
	 * Registers a callback to be triggered whenever a drag input event is detected.
	 */
	@safe
	public void onDragInput(DragHandler dragHandler) {
		this.dragHandlers.push(dragHandler);
	}

	/**
	 * Registers a callback to be triggered whenever a zoom input event is detected.
	 */
	@safe
	public void onZoomInput(ZoomHandler zoomHandler) {
		this.zoomHandlers.push(zoomHandler);
	}

	/**
	 * Presents the current queued render events to the display, using `camera` as the viewport.
	 */
	@trusted @nogc
	public void present(in Camera camera) {
		rl.Camera3D rlCamera = {
			position: rl.Vector3(camera.origin.x, camera.origin.y, camera.origin.z),
			target: rl.Vector3(camera.target.x, camera.target.y, camera.target.z),
			up: rl.Vector3(0.0f, 1.0f, 0.0f),
			fovy: 75.0f,
			type: rl.CameraType.CAMERA_PERSPECTIVE
		};

		rl.BeginDrawing();
		{
			rl.ClearBackground(rl.Colors.BLACK);
			rl.DrawFPS(5, 5);

			rl.BeginMode3D(rlCamera);
			{
				foreach (ref staticDraw; this.staticDraws.byValue()) {
					StaticModel staticModel = staticDraw.model;
					immutable (Matrix) transform = (staticDraw.transform * staticModel.transform);

					foreach (ref mesh; staticModel.meshes) {
						rl.rlDrawMesh(
							mesh.data,
							mesh.material.data,
							*cast(immutable (rl.Matrix4)*)&transform
						);
					}
				}

				this.staticDraws.clear();

				foreach (ref animatedDraw; this.animatedDraws.byValue()) {
					enum scale = 1;

					rl.DrawModel(
						animatedDraw.model.data,
						*cast(rl.Vector3*)&animatedDraw.position,
						scale,
						rl.Colors.WHITE
					);
				}

				this.animatedDraws.clear();
			}
			rl.EndMode3D();
		}
		rl.EndDrawing();
	}

	/**
	 * Processes and dispatches any polled events, returning `true` if the application should
	 * continue or `false` if a request to exit has been made.
	 */
	@trusted @nogc
	public bool process() {
		if (!rl.WindowShouldClose()) {
			// Mouse drag events.
			{
				enum middleMouse = rl.MouseButton.MOUSE_MIDDLE_BUTTON;
				immutable dragPositionNow = Vector2(rl.GetMouseX(), rl.GetMouseY());

				if (rl.IsMouseButtonDown(middleMouse)) {
					rl.HideCursor();

					immutable (Vector2) relativeDrag = (this.dragPosition - dragPositionNow);

					foreach (dragHandler; dragHandlers.byValue()) {
						dragHandler(relativeDrag);
					}
				}

				if (rl.IsMouseButtonReleased(middleMouse)) {
					rl.ShowCursor();
				}

				this.dragPosition = dragPositionNow;
			}

			// Mouse zoom events.
			{
				immutable (float) mouseWheelRelative = rl.GetMouseWheelMove();

				// Float to zero comparison is fine here because there's never any arithmetic done
				// on the floats in this function.
				if (mouseWheelRelative != 0) {
					foreach (zoomHandler; this.zoomHandlers.byValue()) {
						zoomHandler(mouseWheelRelative);
					}
				}
			}

			return true;
		}

		return false;
	}

	/**
	 * Reads the time between the current and last frame.
	 *
	 * Typically, this information is used as a scalar modifier for frame-rate independent events.
	 */
	@trusted @nogc
	public float readFrameTime() {
		return rl.GetFrameTime();
	}

	@trusted @nogc
	private void refreshPointLights() {
		immutable (int) lightCount = (cast(int)this.pointLights.count());

		rl.SetShaderValue(
			this.standardShader,
			rl.GetShaderLocation(this.standardShader, "pointLightCount"),
			&lightCount,
			rl.ShaderUniformDataType.UNIFORM_INT
		);

		uint i = 0;

		foreach (ref pointLight; this.pointLights.byValue()) {
			pointLight.refresh(this, i);

			i += 1;
		}
	}

	/**
	 * Updates the global ambient light value to `color`, where `Color.white` is full illumination
	 * and `Color.black` is none.
	 */
	@trusted @nogc
	public void updateAmbientLight(in Color color) {
		float[4] colorNormalizedValues = color.normalized().values();

		rl.SetShaderValue(
			this.standardShader,
			rl.GetShaderLocation(this.standardShader, "ambient"),
			colorNormalizedValues.ptr,
			rl.ShaderUniformDataType.UNIFORM_VEC4
		);
	}

	/**
	 * Updates the originating position and color of the light at `lightKey` to `origin` and `color`
	 * respectively.
	 *
	 * An assertion is raised if `lightKey` does not reference a point light.
	 */
	@trusted @nogc
	public void updatePointLight(in LightKey lightKey, in Vector3 origin, in Color color) {
		assert(
			lightKey.type == LightType.point,
			"Attempt to modify non-point light using GraphicsServer.updatePointLight"
		);

		immutable (Optional!uint) lightIndex = this.pointLights.indexOf(lightKey.data);

		if (lightIndex.hasValue()) {
			PointLight* light = (&this.pointLights[lightKey.data]);
			(*light) = PointLight(origin, color.normalized());

			light.refresh(this, lightIndex.value());
		}
	}
}
