module aqvi.system.assets;

private import
	aqvi.image,
	aqvi.lina,
	aqvi.memory,
	aqvi.system.engine,
	rl = raylib,
	std.algorithm,
	std.string;

/**
 * GPU-bound dynamic model data composed of one or more meshes and [Material]s.
 *
 * Unlike [StaticModel]s, [AnimatedModel]s are capable of playing animations.
 */
public final class AnimatedModel {
	package Buffer!Material modelMaterials;

	package rl.Model data;

	private this() {

	}

	public ~this() {
		// TODO: Support conditional unloading of materials based on whether or not there are
		// external materials assigned.
		rl.UnloadModel(this.data);
	}

	/**
	 * Loads and returns an [AnimatedModel] instance from the file location at `path`.
	 *
	 * A `transformOverride` not equal to [Matrix.identity] will override the default transformation
	 * applied to the [AnimatedModel] every time it is rendered, provided that the renderer respects
	 * the transform override.
	 *
	 * Additionally, the default materials loaded by the model may be overridden and replaced via
	 * `materialOverrides`, with the order of the supplied [Material]s mapping directly to the
	 * mesh order of the [StaticModel].
	 *
	 * Attempting to call this function before [Engine.initialize] is undefined behavior.
	 */
	@trusted
	public static AnimatedModel loadFile(
		in string path,
		in Matrix transformOverride,
		Material[] materialOverrides
	) {
		auto model = new AnimatedModel();
		model.data = rl.LoadModel(toStringz(path));
		model.data.transform = (*cast(rl.Matrix*)&transformOverride);
		// TODO: Material overrides.

		return model;
	}
}

/**
 * GPU-bound color and vertex modifier data.
 */
public final class Material {
	package Texture[rl.MAX_MATERIAL_MAPS] materialTextures;

	package rl.MaterialMap[rl.MAX_MATERIAL_MAPS] materialMaps;

	package rl.Material data;

	@trusted @nogc
	private this() {
		// Rather than allocate the material map dynamically, just use the address space of the
		// class.
		this.data.maps = this.materialMaps.ptr;
	}

	@trusted @nogc
	public ~this() {
		// UnloadMaterial can't be used as it unconditionally unloads associated textures as well.

		// Check each texture for external ownership and free if there's none.
		foreach (i; 0 .. rl.MAX_MATERIAL_MAPS) {
			if (this.materialTextures[i] is null) {
				rl.UnloadTexture(this.data.maps[i].texture);
			}
		}

		// TODO: Shader unloading conditional to ownership once a shader type abstraction is
		// written.
	}

	/**
	 * Loads and returns a [Material] instance that does not respond to lighting, with
	 * `albedoTexture` as its assigned albedo texture and `albedoTint` as the color modulation
	 * applied atop `albedoTexture`.
	 *
	 * Attempting to call this function before [Engine.initialize] is undefined behavior.
	 */
	@trusted
	public static Material loadUnlit(Texture albedoTexture, in Color albedoTint) {
		auto material = new Material();
		material.data.shader = rl.GetShaderDefault();

		material.data.maps[rl.MaterialMapType.MAP_ALBEDO] = rl.MaterialMap(
			albedoTexture.data,
			rl.Color(albedoTint.r, albedoTint.g, albedoTint.b, albedoTint.a),
			0
		);

		material.materialTextures[rl.MaterialMapType.MAP_ALBEDO] = albedoTexture;

		return material;
	}

	/**
	 * Loads and returns a [Material] instance that exhibits basic ambient, diffuse, and specular
	 * lighting - with `albedoTexture` and `albedoTint` as the albedo texture and color modulation
	 * as well as `metallicTexture` and `normalTexture` as the metallic shine and per-pixel lighting
	 * normal textures.
	 *
	 * Attempting to call this function before [Engine.initialize] is undefined behavior.
	 */
	@trusted
	public static Material loadStandard(
		Texture albedoTexture,
		in Color albedoTint,
		Texture metallicTexture,
		Texture normalTexture
	) {
		auto material = new Material();
		material.data.shader = Engine.standardShader;

		material.data.maps[rl.MaterialMapType.MAP_ALBEDO] = rl.MaterialMap(
			albedoTexture.data,
			rl.Color(albedoTint.r, albedoTint.g, albedoTint.b, albedoTint.a)
		);

		material.data.maps[rl.MaterialMapType.MAP_METALNESS].texture = metallicTexture.data;
		material.data.maps[rl.MaterialMapType.MAP_NORMAL].texture = normalTexture.data;

		material.materialTextures[rl.MaterialMapType.MAP_ALBEDO] = albedoTexture;
		material.materialTextures[rl.MaterialMapType.MAP_METALNESS] = metallicTexture;
		material.materialTextures[rl.MaterialMapType.MAP_NORMAL] = normalTexture;

		return material;
	}
}

/**
 * GPU-bound static model data composed of one or more meshes and [Material]s.
 *
 * Compared to [ComplexModel]s, [StaticModel]s are very lightweight to render.
 */
public final class StaticModel {
	package struct Mesh {
		Material material;

		rl.Mesh data;
	}

	package Buffer!Mesh meshes;

	package Matrix transform = Matrix.identity;

	@safe @nogc
	private this() {

	}

	@trusted @nogc
	public ~this() {
		foreach (ref mesh; this.meshes) {
			rl.UnloadMesh(mesh.data);
		}

		this.meshes.dispose();
	}

	/**
	 * Loads and returns a [StaticModel] instance from the file location at `path`.
	 *
	 * A `transformOverride` not equal to [Matrix.identity] will override the default transformation
	 * applied to the [StaticModel] every time it is rendered, provided that the renderer respects
	 * the transform override.
	 *
	 * Additionally, the default materials loaded by the model may be overridden and replaced via
	 * `materialOverrides`, with the order of the supplied [Material]s mapping directly to the
	 * mesh order of the [StaticModel].
	 *
	 * Attempting to call this function before [Engine.initialize] is undefined behavior.
	 */
	@trusted
	public static StaticModel loadFile(
		in string path,
		in Matrix transformOverride,
		Material[] materialOverrides
	) {
		rl.Model sourceModel = rl.LoadModel(toStringz(path));
		auto tileModel = new StaticModel();
		tileModel.transform = transformOverride;

		// Steal meshes from source model.
		{
			tileModel.meshes.request(sourceModel.meshCount);

			foreach (i; 0 .. sourceModel.meshCount) {
				tileModel.meshes[i].data = sourceModel.meshes[i];
			}

			sourceModel.meshCount = 0;
		}

		// Apply any material overrides then steal any remaining materials that are left unspecified
		// by the overrides from the source model.
		{
			// Mesh count instead of material count because materials are indexed for data
			// normalization: mesh material index -> material pool -> material.
			immutable (size_t) materialOverrideCount = min(
				tileModel.meshes.length,
				materialOverrides.length
			);

			size_t materialCursor = 0;

			while (materialCursor < materialOverrideCount) {
				tileModel.meshes[materialCursor].material = materialOverrides[materialCursor];
				materialCursor += 1;
			}

			while (materialCursor < sourceModel.meshCount) {
				auto material = new Material();

				rl.Material* meshMaterial =
					&sourceModel.materials[sourceModel.meshMaterial[materialCursor]];

				material.data = (*meshMaterial);
				(*meshMaterial) = rl.Material.init;
				tileModel.meshes[materialCursor].material = material;
				materialCursor += 1;
			}
		}

		rl.UnloadModel(sourceModel);

		return tileModel;
	}
}

/**
 * GPU-bound color data asset.
 */
public final class Texture {
	package rl.Texture data;

	@safe @nogc
	private this() {

	}

	@trusted @nogc
	public ~this() {
		rl.UnloadTexture(this.data);
	}

	/**
	 * Attempts to load and return texture data from the location specified by `path`, using a
	 * fallback [Texture] if the requested resource was not found.
	 *
	 * Attempting to call this function before [Engine.initialize] is undefined behavior.
	 */
	@trusted
	public static Texture loadFile(in string path) {
		auto texture = new Texture();
		texture.data = rl.LoadTexture(toStringz(path));

		rl.GenTextureMipmaps(&texture.data);

		return texture;
	}
}
