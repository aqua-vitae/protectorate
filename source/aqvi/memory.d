/**
 * Low-level memory concepts that allow for avoidance of the runtime garbage collector.
 */
module aqvi.memory;

private import core.memory, std.traits;

/**
 * Globally available allocation strategies.
 */
public enum Allocator {
	/**
	 * Allocations that may exist for a non-determinant amount of time, only bound by the lifetime
	 * of the process.
	 */
	persistent,
}

/**
 * Weak-owning memory buffer construct that requires manual memory management.
 *
 * Any allocation strategy specified in [Allocator] may be used with the type. By default, [Buffer]
 * uses [Allocator.persistent].
 */
public struct Buffer(Type) {
	public alias opDollar = length;

	private size_t length_;

	private Type* pointer;

	private Allocator allocator = Allocator.persistent;

	/**
	 * Assigns `allocator` as the memory buffer allocation strategy.
	 */
	@safe @nogc
	public this(in Allocator allocator) pure {
		this.allocator = allocator;
	}

	/**
	 * Disposes of any referenced memory resources.
	 *
	 * Note that this function **does not** call any destructors on its memory contents. Any
	 * destruction semantics must be manually invoked using the `__xdtor`.
	 */
	@system @nogc
	public void dispose() {
		static if (hasIndirections!Type) {
			GC.removeRange(this.pointer);
		}

		final switch (allocator) {
			case Allocator.persistent: pureFree(this.pointer);
		}

		this = this.init;
	}

	/**
	 * Returns the number of `Type` elements contained.
	 */
	@safe @nogc
	public size_t length() const {
		return this.length_;
	}

	/**
	 * An unsafe and unchecked accesses of raw memory allocation at `index`, returning whatever data
	 * is held there as `Type`.
	 */
	@system @nogc
	public ref inout (Type) opIndex(in size_t index) inout pure {
		assert(index < this.length_, "Buffer index out of bounds");

		return this.pointer[index];
	}

	/**
	 * Returns a weak reference to the memory contents from `index` to `length`.
	 *
	 * Note that the slice returned does not own the memory, but rather acts as an ephemeral view.
	 * Any subsequent call to [Buffer.request] or [Buffer.dispose] thereafter will invalidate the
	 * returned value.
	 *
	 * This function is considered unsafe for the above reason.
	 */
	@system @nogc
	public inout (Type)[] opSlice() inout pure {
		return this.pointer[0 .. this.length_];
	}

	/**
	 * Returns a weak reference to the memory contents from `index` to `length`, asserting if
	 * neither of the inputs are within the range of the memory buffer.
	 *
	 * Note that the slice returned does not own the memory, but rather acts as an ephemeral view.
	 * Any subsequent call to [Buffer.request] or [Buffer.dispose] thereafter will invalidate the
	 * returned value.
	 *
	 * This function is considered unsafe for the above reason.
	 */
	@system @nogc
	public inout (Type)[] opSlice(in size_t index, in size_t length) inout pure {
		assert(
			(index < this.length_) && (length <= this.length_) && (index < length),
			"Buffer slice out of bounds"
		);

		return this.pointer[index .. length];
	}

	/**
	 * Requests enough space for storing `length` number of `Type`s, returning `true` if the request
	 * was successful, otherwise `false`.
	 *
	 * If the function is called again, the buffer is reallocated to the new length, truncating
	 * itself if necessary. It is not necessary to call [Buffer.dispose] before calling
	 * [Buffer.request] again.
	 */
	@system @nogc
	public bool request(in size_t length) {
		immutable (size_t) allocationSize = (Type.sizeof * length);

		final switch (allocator) {
			case Allocator.persistent:
				this.pointer = (cast(Type*)pureRealloc(this.pointer, allocationSize));
		}

		immutable (bool) requestSuccess = (this.pointer != null);
		this.length_ = (length * requestSuccess);

		static if (hasIndirections!Type) {
			GC.addRange(this.pointer, allocationSize);
		}

		return requestSuccess;
	}
}
