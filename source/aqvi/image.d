/**
 * CPU-bound image data manipulation utilies.
 */
module aqvi.image;

import aqvi.lina;

/**
 * 32-bit 16,581,375 color encoding with alpha channel.
 */
public struct Color {
	enum {
		/**
		 * Absolute black constant.
		 */
		black = Color(0, 0, 0),

		/**
		 * Absolute blue constant.
		 */
		blue = Color(0, 0, 255),

		/**
		 * Absolute green constant.
		 */
		green = Color(0, 255, 0),

		/**
		 * Absolute red constant.
		 */
		red = Color(255, 0, 0),

		/**
		 * Absolute white constant.
		 */
		white = Color(255, 255, 255),
	}

	/**
	 * Red color channel.
	 */
	ubyte r;

	/**
	 * Green color channel.
	 */
	ubyte g;

	/**
	 * Blue color channel.
	 */
	ubyte b;

	/**
	 * Transparency color channel.
	 */
	ubyte a;

	/**
	 * Assigns `red`, `green`, and `blue` as the red, green, and blue color values with an opaque
	 * alpha.
	 */
	@safe @nogc
	this(in ubyte red, in ubyte green, in ubyte blue) pure {
		this(red, green, blue, ubyte.max);
	}

	/**
	 * Assigns `red`, `green`, `blue`, and `alpha` as the red, green, blue, and alpha values
	 * respectively.
	 */
	@safe @nogc
	this(in ubyte red, in ubyte green, in ubyte blue, in ubyte alpha) pure {
		this.r = red;
		this.g = green;
		this.b = blue;
		this.a = alpha;
	}

	/**
	 * Returns a darkened version of the r, g, b color values.
	 */
	@safe @nogc
	Color darkened(in float value) const pure {
		return Color(
			cast(ubyte)((cast(float)this.r) - (this.r * value)),
			cast(ubyte)((cast(float)this.g) - (this.g * value)),
			cast(ubyte)((cast(float)this.b) - (this.b * value)),
			this.a
		);
	}

	/**
	 * Returns a lightened version of the r, g, b color values.
	 */
	@safe @nogc
	Color lightened(in float value) const pure {
		return Color(
			cast(ubyte)(this.r * value),
			cast(ubyte)(this.g * value),
			cast(ubyte)(this.b * value),
			this.a
		);
	}

	/**
	 * Returns the red, green, blue, and alpha channels as a `Vector4` of values ranging between `0`
	 * and `1`.
	 */
	@safe @nogc
	Vector4 normalized() const pure {
		immutable channelMax = (cast(float)ubyte.max);

		return Vector4(
			this.r / channelMax,
			this.g / channelMax,
			this.b / channelMax,
			this.a / channelMax
		);
	}
}
