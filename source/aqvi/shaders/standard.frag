#version 330

// Input vertex attributes (from vertex shader)
in vec3 fragPosition;
in vec2 fragTexCoord;
in vec4 fragColor;
in vec3 fragNormal;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;

// Output fragment color
out vec4 finalColor;

#define MAX_LIGHTS 4
#define LIGHT_DIRECTIONAL 0
#define LIGHT_POINT 1

struct DirectionalLight {
	vec3 origin;

	vec3 target;

	vec4 color;
};

struct PointLight {
	vec3 origin;

	vec4 color;
};

// Input lighting values
uniform DirectionalLight directionalLights[MAX_LIGHTS];

uniform int directionalLightCount;

uniform PointLight pointLights[MAX_LIGHTS];

uniform int pointLightCount;

uniform vec4 ambient;

uniform vec3 viewPos;

void main() {
	// Texel color fetching from texture sampler
	vec4 texelColor = texture(texture0, fragTexCoord);
	vec3 lightDot = vec3(0.0);
	vec3 normal = normalize(fragNormal);
	vec3 viewD = normalize(viewPos - fragPosition);
	vec3 specular = vec3(0.0);

	for (int i = 0; i < directionalLightCount; i += 1) {
		vec3 light = (-normalize(directionalLights[i].target - directionalLights[i].origin));
		float NdotL = max(dot(normal, light), 0.0);
		lightDot += (directionalLights[i].color.rgb * NdotL);
		float specCo = 0.0;

		if (NdotL > 0.0) {
			// 16 refers to shine.
			specCo = pow(max(0.0, dot(viewD, reflect(-(light), normal))), 16.0);
		}

		specular += specCo;
	}

	for (int i = 0; i < pointLightCount; i += 1) {
		vec3 light = normalize(pointLights[i].origin - fragPosition);
		float NdotL = max(dot(normal, light), 0.0);
		lightDot += (pointLights[i].color.rgb * NdotL);
		float specCo = 0.0;

		if (NdotL > 0.0) {
			// 16 refers to shine.
			specCo = pow(max(0.0, dot(viewD, reflect(-(light), normal))), 16.0);
		}

		specular += specCo;
	}

	finalColor = (texelColor * ((colDiffuse + vec4(specular, 1.0)) * vec4(lightDot, 1.0)));
	finalColor += (texelColor * (ambient / 10.0) * colDiffuse);

	// Gamma correction
	finalColor = pow(finalColor, vec4(1.0/2.2));
}
