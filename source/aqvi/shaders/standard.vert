#version 330

// Input vertex attributes
in vec3 vertexPosition;
in vec2 vertexTexCoord;
in vec3 vertexNormal;
in vec4 vertexColor;

// Input uniform values
uniform mat4 mvp;
uniform mat4 matModel;

// Output vertex attributes (to fragment shader)
out vec3 fragPosition;
out vec2 fragTexCoord;
out vec4 fragColor;
out vec3 fragNormal;

void main() {
	// TODO: prefer matNormal uniform once updated to Raylib 3.7.
	mat3 matNormal = transpose(inverse(mat3(matModel)));

	// Send vertex attributes to fragment shader
	fragPosition = vec3(matModel * vec4(vertexPosition, 1.0));
	fragTexCoord = vertexTexCoord;
	fragColor = vertexColor;
    fragNormal = normalize(matNormal*vertexNormal);

	// Calculate final vertex position
	gl_Position = (mvp * vec4(vertexPosition, 1.0));
}
