/**
 * Unordered key-based data structures and utility functions.
 *
 * Implementations derived from descriptions given by Alan Deutsch
 * [http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/p0661r0.pdf].
 */
module aqvi.collections.slotmap;

private import aqvi.memory, aqvi.types, std.traits;

/**
 * Strong type definition for [PackedSlotMap] access keys.
 */
public struct SlotKey {
	private uint index;

	private uint generation;
}

/**
 * Densely packed map of `ValueType` indexed by [SlotKey], ensuring O(1) insertion, removal, and
 * lookup, as well as efficient value iteration.
 */
public final class PackedSlotMap(ValueType) {
	private struct Slot {
		uint generation;

		uint indexOrFree;
	}

	private uint freeHead = 0;

	private uint keyValueCount = 0;

	private Buffer!ValueType valueBuffer;

	private Buffer!SlotKey keyBuffer;

	private Buffer!Slot slotBuffer;

	/**
	 * Assigns `allocator` as the allocation strategy.
	 */
	@trusted @nogc
	public this(in Allocator allocator) {
		this.slotBuffer = allocator;
		this.valueBuffer = allocator;
		this.keyBuffer = allocator;
	}

	/**
	 * Assigns `allocator` as the allocation strategy and `initialCapacity` as the initial capacity.
	 */
	@trusted @nogc
	public this(in Allocator allocator, in uint initialCapacity) {
		this(allocator);

		assert(
			this.slotBuffer.request(initialCapacity) &&
			this.valueBuffer.request(initialCapacity) &&
			this.keyBuffer.request(initialCapacity),
			"PackedSlotMap construction out of memory"
		);
	}

	@trusted @nogc
	public ~this() {
		static if (is(ValueType == struct) && hasElaborateDestructor!ValueType) {
			foreach (ref value; this.valueBuffer[0 .. this.keyValueCount]) {
				value.__xdtor();
			}
		}

		this.slotBuffer.dispose();
		this.valueBuffer.dispose();
		this.keyBuffer.dispose();
	}

	/**
	 * Returns a mutable forward range of the contained values.
	 */
	@safe @nogc
	public auto byValue() pure {
		struct ValueRange {
			private PackedSlotMap slotMap;

			private uint iteration = 0;

			@disable
			this();

			@safe @nogc
			public this(PackedSlotMap slotMap) {
				this.slotMap = slotMap;
			}

			@safe @nogc
			public bool empty() const {
				return (this.iteration == this.slotMap.keyValueCount);
			}

			public ref ValueType front() {
				return this.slotMap.valueBuffer[this.iteration];
			}

			@safe @nogc
			public void popFront() {
				this.iteration += 1;
			}
		}

		return ValueRange(this);
	}

	/**
	 * Returns a constant forward range of the contained values.
	 */
	@safe @nogc
	public auto byValue() const pure {
		struct ValueRange {
			private const (PackedSlotMap) slotMap;

			private uint iteration = 0;

			@disable
			this();

			@safe @nogc
			public this(const (PackedSlotMap) slotMap) {
				this.slotMap = slotMap;
			}

			@safe @nogc
			public bool empty() const {
				return (this.iteration == this.slotMap.keyValueCount);
			}

			public ref const (ValueType) front() const {
				return this.slotMap.valueBuffer[this.iteration];
			}

			@safe @nogc
			public void popFront() {
				this.iteration += 1;
			}
		}

		return ValueRange(this);
	}

	/**
	 * Returns the pre-allocated capacity for more items.
	 */
	@safe @nogc
	public uint capacity() const pure {
		return (cast(uint)this.keyBuffer.length());
	}

	/**
	 * Clears all keys and values, calling the elaborate destructors of any contained values in the
	 * process.
	 */
	@trusted @nogc
	public void clear() pure {
		static if (is(ValueType == struct) && hasElaborateDestructor!ValueType) {
			foreach (ref value; this.valueBuffer[0 .. this.keyValueCount]) {
				value.__xdtor();
			}
		}

		while (this.keyValueCount) {
			// Each allocated slot needs to be "freed" or unregistered so that it may be re-used in
			// the future, otherwise clearing creates a slot leak.
			this.keyValueCount -= 1;
			immutable (uint) keyIndex = this.keyBuffer[this.keyValueCount].index;

			if (keyIndex < this.capacity()) {
				this.freeSlot(keyIndex);
			}
		}
	}

	/**
	 * Returns `true` if a value mapped to `key` exists, otherwise `false`.
	 */
	@trusted @nogc
	public bool contains(in SlotKey key) const pure {
		// Slots are not stored contiguously and may be arranged sparsely anywhere between 0 and the
		// current slot map capacity.
		if (key.index < this.capacity()) {
			immutable (Slot) slot = this.slotBuffer[key.index];

			return (slot.generation == key.generation);
		}

		return false;
	}

	/**
	 * Returns the number of items contained.
	 */
	@safe @nogc
	public uint count() const pure {
		return this.keyValueCount;
	}

	/**
	 * Inserts `value`, returning a key for accessing it and asserting if the limit of `uint.max`
	 * items is reached.
	 *
	 * If the current capacity is reached, an implicit call to [PackedSlotMap.reserve] is made,
	 * growing by a factor of `2`.
	 */
	@trusted @nogc
	public SlotKey insert(ValueType value) {
		return this.insert(value);
	}

	/**
	 * Inserts `value` by reference, returning a key for accessing it and asserting if the limit of
	 * `uint.max` items is reached.
	 *
	 * If the current capacity is reached, an implicit call to [PackedSlotMap.reserve] is made,
	 * growing by a factor of `2`.
	 */
	@trusted @nogc
	public SlotKey insert(ref ValueType value) {
		assert(this.keyValueCount < uint.max, "PackedSlotMap insertion overflow");

		if (this.freeHead < this.keyValueCount) {
			// Re-usable space in the free list.
			Slot* slot = (&this.slotBuffer[this.freeHead]);
			immutable (uint) occupiedGeneration = (slot.generation | 1);

			immutable (SlotKey) key = {
				index: this.freeHead,
				generation: occupiedGeneration
			};

			this.valueBuffer[this.keyValueCount] = value;
			this.keyBuffer[this.keyValueCount] = key;
			this.keyValueCount += 1;
			this.freeHead = slot.indexOrFree;
			slot.indexOrFree = (this.keyValueCount - 1);
			slot.generation = occupiedGeneration;

			return key;
		}

		immutable (uint) capacity = (cast(uint)this.capacity());

		if (this.keyValueCount == capacity) {
			// Filled the existing capacity, get more.
			this.reserve((capacity == 0) ? 2 : capacity);
		}

		immutable (SlotKey) key = {
			index: this.freeHead,
			generation: 1
		};

		this.valueBuffer[this.keyValueCount] = value;
		this.keyBuffer[this.keyValueCount] = key;
		this.slotBuffer[this.keyValueCount] = Slot(1, this.keyValueCount);
		this.keyValueCount += 1;
		this.freeHead = this.keyValueCount;

		return key;
	}

	@trusted @nogc
	private uint freeSlot(in uint slotIndex) pure {
		// Utility function for de-registering slots as being used, freeing them for future re-use
		// via the free list.
		Slot* slot = (&this.slotBuffer[slotIndex]);
		immutable (uint) valueIndex = slot.indexOrFree;
		slot.generation += 1;
		slot.indexOrFree = this.freeHead;
		this.freeHead = slotIndex;

		return valueIndex;
	}

	/**
	 * Returns the index currently assigned to `key` or [none] if it is invalid.
	 *
	 * Not that, any other non-const member function called after this function has the chance to
	 * invalidate the returned index value, due to the packing requirements of the data structure.
	 */
	@trusted @nogc
	public Optional!uint indexOf(in SlotKey key) const pure {
		immutable (Slot) slot = this.slotBuffer[key.index];

		if (slot.generation == key.generation) {
			return Optional!uint(slot.indexOrFree);
		}

		return none!uint();
	}

	/**
	 * Returns the value associated with `key`, asserting if it is invalid.
	 */
	@trusted @nogc
	public ref inout (ValueType) opIndex(in SlotKey key) inout pure {
		immutable (Slot) slot = this.slotBuffer[key.index];

		assert(slot.generation == key.generation, "PackedSlotMap invalid key index");

		return this.valueBuffer[slot.indexOrFree];
	}

	/**
	 * Removes the value associated with `key`, asserting if it is invalid.
	 */
	@trusted @nogc
	public void remove(in SlotKey key) pure {
		immutable (uint) valueIndex = this.freeSlot(key.index);

		// Element swap "removal" for constant time key and value removal.
		this.keyValueCount -= 1;
		this.keyBuffer[valueIndex] = this.keyBuffer[this.keyValueCount];
		this.valueBuffer[valueIndex] = this.valueBuffer[this.keyValueCount];

		if (valueIndex < this.keyValueCount) {
			// If something took the place of the previous item, update slot to the new position.
			this.slotBuffer[this.keyBuffer[valueIndex].index].indexOrFree = valueIndex;
		}
	}

	/**
	 * Grows the item capacity for more elements by `additionalCapacity` number of elements,
	 * asserting if any of the buffers failed to re-allocate.
	 */
	@trusted @nogc
	public void reserve(in uint additionalCapacity) {
		assert(
			this.slotBuffer.request(this.slotBuffer.length() + additionalCapacity) &&
			this.valueBuffer.request(this.valueBuffer.length() + additionalCapacity) &&
			this.keyBuffer.request(this.keyBuffer.length() + additionalCapacity),
			"PackedSlotMap reservation out of memory"
		);
	}
}


unittest {
	int[4] testingValues = [6969, 420, 69, 1000];

	// Standard init.
	{
		auto slotMap = new PackedSlotMap!int(Allocator.persistent);

		assert(slotMap.count() == 0);
		assert(slotMap.capacity() == 0);
	}

	// Custom capacity init.
	{
		enum customCapacity = 12;
		auto slotMap = new PackedSlotMap!int(Allocator.persistent, customCapacity);

		assert(slotMap.count() == 0);
		assert(slotMap.capacity() == customCapacity);
	}

	// Mutation.
	{
		auto slotMap = new PackedSlotMap!int(Allocator.persistent);

		// First two elements should insert without increasing the default capacity.
		foreach (i, value; testingValues[0 .. 2]) {
			immutable (SlotKey) key = slotMap.insert(value);

			assert(slotMap[key] == value);
			assert(slotMap.contains(key));
			assert(slotMap.indexOf(key).value() == i);
			assert(slotMap.count() == (i + 1));
			assert(slotMap.capacity() == 2);
		}

		// Next two elements will re-allocate once but never more after, as the capacity will grow
		// to the default capacity multiplied by 2.
		immutable (SlotKey) key3 = slotMap.insert(420);

		assert(slotMap[key3] == 420);
		assert(slotMap.contains(key3));
		assert(slotMap.indexOf(key3).value() == 2);
		assert(slotMap.count() == 3);
		assert(slotMap.capacity() == 4);

		immutable (SlotKey) key4 = slotMap.insert(1000);

		assert(slotMap[key4] == 1000);
		assert(slotMap.contains(key4));
		assert(slotMap.indexOf(key4).value() == 3);
		assert(slotMap.count() == 4);
		assert(slotMap.capacity() == 4);

		// Remove from the middle of the slot map at index 3 to see how it handles mid-buffer
		// removal [_ _ x _].
		slotMap.remove(key3);

		assert(slotMap[key4] == 1000);

		assert(!slotMap.contains(key3));
		assert(slotMap.indexOf(key4).value() == 2);
		assert(slotMap.count() == 3);
		assert(slotMap.capacity() == 4);

		// Remove the tail element that will have been shifted down by 1 from the previous removal
		// [_ _ x x].
		slotMap.remove(key4);

		assert(!slotMap.contains(key3));
		assert(!slotMap.contains(key4));
		assert(slotMap.count() == 2);
		assert(slotMap.capacity() == 4);

		// Clear all of the elements from the list to see how it handles being reset [x x x x].
		slotMap.clear();

		assert(slotMap.count() == 0);
		assert(slotMap.capacity() == 4);

		// Grow the buffer by an additional 2 elements in size to see how it handles growing its
		// capacity.
		slotMap.reserve(2);

		assert(slotMap.count() == 0);
		assert(slotMap.capacity() == 6);
	}

	// Iteration
	{
		auto slotMap = new PackedSlotMap!int(Allocator.persistent);

		foreach (value; testingValues) {
			slotMap.insert(value);
		}

		auto valuesRange = slotMap.byValue();

		assert(!valuesRange.empty());
		assert(valuesRange.front() == testingValues[0]);

		uint iteration = 0;

		foreach (value; valuesRange) {
			assert(value == testingValues[iteration]);

			iteration += 1;
		}
	}
}
