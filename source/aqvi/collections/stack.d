/**
 * Last-in-first-out data structures and utility functions.
 */
module aqvi.collections.stack;

private import aqvi.memory, aqvi.types, std.traits;

/**
 * Densely packed stack of `ValueType` that exposes array-like random access semantics, ensuring
 * O(1) pushes, removal, and random access, as well as efficient value iteration.
 */
public final class PackedStack(ValueType) {
	public alias opDollar = count;

	private Buffer!ValueType valueBuffer;

	private uint valueCount;

	/**
	 * Assigns `allocator` as the allocation strategy.
	 */
	@trusted @nogc
	public this(in Allocator allocator) {
		this.valueBuffer = allocator;
	}

	/**
	 * Assigns `allocator` as the allocation strategy and `initialCapacity` as the initial capacity.
	 */
	@trusted @nogc
	public this(in Allocator allocator, in size_t initialCapacity) {
		this(allocator);
		assert(this.valueBuffer.request(initialCapacity), "PackedStack construction out of memory");
	}

	@trusted @nogc
	public ~this() {
		static if (is(ValueType == struct) && hasElaborateDestructor!ValueType) {
			foreach (ref value; this.valueBuffer[0 .. this.valueCount]) {
				value.__xdtor();
			}
		}

		this.valueBuffer.dispose();
	}

	/**
	 * Returns a mutable forward range of the contained values.
	 */
	@safe @nogc
	public auto byValue() pure {
		struct ValueRange {
			private PackedStack packedStack;

			private uint iteration = 0;

			@disable
			this();

			@safe
			public this(PackedStack packedStack) {
				this.packedStack = packedStack;
			}

			@safe
			public bool empty() const {
				return (this.iteration == this.packedStack.valueCount);
			}

			public ref ValueType front() {
				return this.packedStack.valueBuffer[this.iteration];
			}

			@safe
			public void popFront() {
				this.iteration += 1;
			}
		}

		return ValueRange(this);
	}

	/**
	 * Returns a constant forward range of the contained values.
	 */
	@safe @nogc
	public auto byValue() const pure {
		struct ValueRange {
			private const (PackedStack) packedStack;

			private uint iteration = 0;

			@disable
			this();

			@safe @nogc
			public this(const (PackedStack) packedStack) {
				this.packedStack = packedStack;
			}

			@safe @nogc
			public bool empty() const {
				return (this.iteration == this.packedStack.valueCount);
			}

			public ref const (ValueType) front() const {
				return this.packedStack.valueBuffer[this.iteration];
			}

			@safe @nogc
			public void popFront() {
				this.iteration += 1;
			}
		}

		return ValueRange(this);
	}

	/**
	 * Returns the pre-allocated capacity for more values.
	 */
	@safe @nogc
	public uint capacity() const pure {
		return (cast(uint)this.valueBuffer.length());
	}

	/**
	 * Returns the number of values contained.
	 */
	@safe @nogc
	public uint count() const pure {
		return this.valueCount;
	}

	/**
	 * Clears all values, calling the elaborate destructors of any contained values in the process.
	 */
	@safe @nogc
	public void clear() pure {
		static if (is(ValueType == struct) && hasElaborateDestructor!ValueType) {
			foreach (ref value; this.valueBuffer[0 .. this.valueCount]) {
				value.__xdtor();
			}
		}

		this.valueCount = 0;
	}

	/**
	 * Returns the value at `index`, asserting if `index` is not less than [PackedStack.count].
	 */
	@trusted @nogc
	public ref inout (ValueType) opIndex(in uint index) inout pure {
		assert(index < this.valueCount, "PackedStack index out of bounds");

		return this.valueBuffer[index];
	}

	/**
	 * Attempts to pop the top-most value, returning it or [none] if [PackedStack.count] is `0`.
	 */
	public Optional!ValueType pop() {
		if (this.valueCount != 0) {
			this.valueCount -= 1;

			return Optional!ValueType(this.valueBuffer[this.valueCount]);
		}

		return Optional!ValueType();
	}

	/**
	 * Pushes `value`, returning a reference to the inserted value and asserting if the limit of
	 * `uint.max` values is reached.
	 *
	 * If the current capacity is reached, an implicit call to [PackedStack.reserve] is made,
	 * growing by a factor of `2`.
	 */
	@trusted @nogc
	public ref ValueType push(ValueType value) {
		return this.push(value);
	}

	/**
	 * Pushes `value` by reference, returning a reference to the inserted value and asserting if the
	 * limit of `uint.max` values is reached.
	 *
	 * If the current capacity is reached, an implicit call to [PackedStack.reserve] is made,
	 * growing by a factor of `2`.
	 */
	@trusted @nogc
	public ref ValueType push(ref ValueType value) {
		assert(this.valueCount < uint.max);

		immutable (uint) valueCapacity = (cast(uint)this.valueBuffer.length());

		if (this.valueCount == valueCapacity) {
			this.reserve((valueCapacity == 0) ? 2 : valueCapacity);
		}

		scope (exit) this.valueCount += 1;

		return (this.valueBuffer[this.valueCount] = value);
	}

	/**
	 * Grows the value capacity for more elements by `additionalCapacity`, asserting if the request
	 * for more memory failed.
	 */
	@trusted @nogc
	public void reserve(in uint additionalCapacity) {
		assert(
			this.valueBuffer.request(this.valueBuffer.length() + additionalCapacity),
			"PackedStack reservation out of memory"
		);
	}
}

unittest {
	// Default capacity init.
	{
		auto stack = new PackedStack!int(Allocator.persistent);

		assert(stack.capacity() == 0);
		assert(stack.count() == 0);
	}

	// Custom capacity init.
	{
		auto stack = new PackedStack!int(Allocator.persistent, 4);

		assert(stack.capacity() == 4);
		assert(stack.count() == 0);
	}

	// Mutation.
	{
		auto stack = new PackedStack!int(Allocator.persistent);

		stack.push(10);

		assert(stack.capacity() == 2);
		assert(stack.count() == 1);
		assert(stack[0] == 10);

		stack.push(20);

		assert(stack.capacity() == 2);
		assert(stack.count() == 2);
		assert(stack[1] == 20);

		stack.push(30);

		assert(stack.capacity() == 4);
		assert(stack.count() == 3);
		assert(stack[2] == 30);

		assert(stack.pop().value() == 30);

		assert(stack.capacity() == 4);
		assert(stack.count() == 2);
		assert(stack[1] == 20);

		stack.reserve(6);

		assert(stack.capacity() == 10);
		assert(stack.count() == 2);
		assert(stack[1] == 20);
	}

	// Iteration
	{
		static immutable testingValues = [10, 20, 30];

		auto stack = new PackedStack!int(Allocator.persistent);

		foreach (testingValue; testingValues) {
			stack.push(testingValue);
		}

		uint iteration = 0;

		foreach (ref value; stack.byValue()) {
			assert(value == testingValues[iteration]);

			iteration += 1;
		}
	}
}
