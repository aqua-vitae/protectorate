module app;

private import
	aqvi.collections.slotmap,
	aqvi.system,
	aqvi.image,
	aqvi.memory,
	aqvi.lina,
	aqvi.types,
	std.algorithm,
	std.typecons;

private struct SceneProp {
	Vector2 position = Vector2.zero;

	AnimatedModel model;

	@disable
	this();

	@safe @nogc
	this(in Vector2 position, AnimatedModel model) pure {
		this.position = position;
		this.model = model;
	}
}

private struct SceneTile {
	Vector2 position = Vector2.zero;

	StaticModel model;

	@disable
	this();

	@safe @nogc
	this(in Vector2 position, StaticModel model) pure {
		this.position = position;
		this.model = model;
	}
}

private StaticModel[] getTileModels() {
	Texture baseAlbedo = Texture.loadFile("tiles/dungeon_wall/base_albedo.png");
	Texture baseMetallic = Texture.loadFile("tiles/dungeon_wall/base_metallic.png");
	Texture baseNormal = Texture.loadFile("tiles/dungeon_wall/base_normal.png");
	Texture trimAlbedo = Texture.loadFile("tiles/dungeon_wall/trim_albedo.png");
	Texture trimMetallic = Texture.loadFile("tiles/dungeon_wall/trim_metallic.png");
	Texture trimNormal = Texture.loadFile("tiles/dungeon_wall/trim_normal.png");

	Texture floorAlbedo = Texture.loadFile("tiles/dungeon_floor/base_albedo.png");
	Texture floorMetallic = Texture.loadFile("tiles/dungeon_floor/base_metallic.png");
	Texture floorNormal = Texture.loadFile("tiles/dungeon_floor/base_normal.png");

	return [
		StaticModel.loadFile("tiles/dungeon_floor/default.glb", Matrix.scaleXyz(0.5f), [
			Material.loadStandard(floorAlbedo, Color.white, floorMetallic, floorNormal),
		]),

		StaticModel.loadFile("tiles/dungeon_wall/corner.glb", Matrix.scaleXyz(0.5f), [
			Material.loadUnlit(baseMetallic, Color.white),
			Material.loadStandard(baseAlbedo, Color.white, baseMetallic, baseNormal),
			Material.loadStandard(trimAlbedo, Color.white, trimMetallic, trimNormal),
		]),

		StaticModel.loadFile("tiles/dungeon_wall/foursided.glb", Matrix.scaleXyz(0.5f), [
			Material.loadUnlit(baseMetallic, Color.white),
			Material.loadStandard(baseAlbedo, Color.white, baseMetallic, baseNormal),
			Material.loadStandard(trimAlbedo, Color.white, trimMetallic, trimNormal),
		]),

		StaticModel.loadFile("tiles/dungeon_wall/nonesided.glb", Matrix.scaleXyz(0.5f), [
			Material.loadUnlit(baseMetallic, Color.white),
		]),

		StaticModel.loadFile("tiles/dungeon_wall/onesided.glb", Matrix.scaleXyz(0.5f), [
			Material.loadStandard(trimAlbedo, Color.white, trimMetallic, trimNormal),
			Material.loadUnlit(baseMetallic, Color.white),
			Material.loadStandard(baseAlbedo, Color.white, baseMetallic, baseNormal),
		]),

		StaticModel.loadFile("tiles/dungeon_wall/parallel.glb", Matrix.scaleXyz(0.5f), [
			Material.loadStandard(baseAlbedo, Color.white, baseMetallic, baseNormal),
		]),

		StaticModel.loadFile("tiles/dungeon_wall/threesided.glb", Matrix.scaleXyz(0.5f), [
			Material.loadUnlit(baseMetallic, Color.white),
			Material.loadStandard(baseAlbedo, Color.white, baseMetallic, baseNormal),
			Material.loadStandard(trimAlbedo, Color.white, trimMetallic, trimNormal),
		]),
	];
}

private int main() {
	Optional!Engine initializedEngine = Engine.initialize(1280, 720, "Protectorate");

	if (initializedEngine.hasValue()) {
		enum cameraOrigin = Vector3(0, 5, 5);

		struct Cell {
			SlotKey tileKey;

			SlotKey propKey;
		}

		Engine engine = initializedEngine.value();
		uint width = 5;
		uint height = 5;
		auto cells = new Cell[width * height];
		Vector3 cameraPosition = cameraOrigin;
		float cameraZoom = 0;
		auto tiles = new PackedSlotMap!SceneTile(Allocator.persistent);
		auto props = new PackedSlotMap!SceneProp(Allocator.persistent);

		void assignProp(in uint x, in uint y, AnimatedModel propModel) {
			cells[(width * y) + x].propKey =
				props.insert(SceneProp(Vector2(x, y), propModel));
		}

		void assignTile(in uint x, in uint y, StaticModel tileModel) {
			cells[(width * y) + x].tileKey =
				tiles.insert(SceneTile(Vector2(x, y), tileModel));
		}

		Camera camera = {
			origin: cameraOrigin,
		};

		StaticModel[] dungeonTileModels = getTileModels();

		engine.onDragInput((dragRelative) {
			immutable (Vector2) dragRelativeDampened = (dragRelative * 0.025f);
			cameraPosition.x += dragRelativeDampened.x;
			cameraPosition.z += dragRelativeDampened.y;
		});

		engine.onZoomInput((zoomRelative) {
			cameraZoom = clamp(cameraZoom - zoomRelative, 0, 10);
		});

		// engine.onPickInput((pickingInfo) {
			// Vector2 groundPosition = pickingInfo.groundPosition();

			// this.assignTile(cast(uint)round(groundPosition.x), cast(uint)round(groundPosition.y), dungeonTileModels[0]);
		// });

		engine.createPointLight(Vector3(2.5f, 1f, 2.5f), Color.white.darkened(0.9f));

		assignTile(0, 0, dungeonTileModels[4]);
		assignTile(1, 0, dungeonTileModels[4]);
		assignTile(2, 0, dungeonTileModels[4]);
		assignTile(3, 0, dungeonTileModels[4]);
		assignTile(4, 0, dungeonTileModels[1]);
		assignTile(0, 1, dungeonTileModels[6]);
		assignTile(4, 4, dungeonTileModels[1]);
		assignProp(1, 1, AnimatedModel.loadFile("props/claypots/1.glb", Matrix.scaleXyz(100) * Matrix.translation(0, 0.005f, 0), []));

		while (engine.process()) {
			immutable (float) frameTime = engine.readFrameTime();
			immutable (float) cameraSpeed = (frameTime * 8);
			Vector3 cameraPositionZoom = (cameraPosition + Vector3(0, cameraZoom, cameraZoom));
			camera.origin = camera.origin.lerp(cameraPositionZoom, cameraSpeed);
			camera.target = camera.target.lerp(cameraPositionZoom - cameraOrigin, cameraSpeed);

			// Draw tiles.
			foreach (ref tile; tiles.byValue()) {
				engine.drawModel(tile.model, Matrix.translation(tile.position.x, 0, tile.position.y));
			}

			// Draw props.
			foreach (ref prop; props.byValue()) {
				engine.drawModel(prop.model, Vector3(prop.position.x, 0, prop.position.y));
			}

			engine.present(camera);
		}
	}

	return 0;
}
