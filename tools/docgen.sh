
shopt -s extglob

pushd "./docs"
	rm -v !("script.js"|"search-docs.js"|"style.css")
popd

mkdir ./docs
dub run adrdox -- ./source --skeleton ./docs/skeleton.html -i --copy-standard-files=false --header-title="Protectorate Docs" --header-link="https://gitlab.com/aqua-vitae/protectorate" -o ./docs
